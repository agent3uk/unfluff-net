# README #

### What is this repository for? ###

Unfluff .NET is a simple to use, yet powerful web scraper. Based off of the Unfluff library originally written in Coffeescript. Unfluff .NET filters out web page noise to get to the juicy text you want. Unfluff will also return other valuable page information such as location, links and media.

### How do I get set up? ###

This repository contains all the necessary files to get going in Visual Studio. Simply clone this repository, open the .sln in Visual Studio 2015 or above and build. There is also a unit test project included in this repository which uses NUnit 2.6.4.

### Usage ###

```ScraperResult result = new PageScraper().Process(html, originalUri);```
or
```ScraperResult result = new PageScraper().Process(stream, originalUri);```

If you wish to specify a language other than English you can pass a 3rd argument into `PageScraper.Process()`, which is a enum of all the currently available languages.

### Contribution guidelines ###

This is an open source library and as such we would be grateful if you could keep code contributions simple and readable for other contributors. Any code changes must have covering tests or will be rejected by the repository administrator.

### Who do I talk to? ###

Unfluff .NET is originally owned by Agent3. If you have any questions regarding this library please drop us an email @ [dev-team@agent3.com](mailto:dev-team@agent3.com)