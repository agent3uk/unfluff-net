﻿using System.Text;

namespace Unfluff.Core.Extensions
{
    internal static class StringExtensions
    {
        internal static string SquishWhitespace(this string input, int characterLimit = -1)
        {
            if (string.IsNullOrWhiteSpace(input)) return string.Empty;

            var builder = new StringBuilder();
            bool lastCharWasSpace = false;
            
            foreach (var character in input)
            {
                if (character == ' ' && lastCharWasSpace == false)
                {
                    builder.Append(character);
                    lastCharWasSpace = true;
                }
                else if (character != ' ')
                {
                    builder.Append(character);
                    lastCharWasSpace = false;
                }

                if (characterLimit != -1 && builder.Length == characterLimit) break;
            }

            return builder.ToString();
        }
    }
}
