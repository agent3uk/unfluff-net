﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;

namespace Unfluff.Core.Extensions
{
    internal static class ScraperExtensions
    {
        private static readonly char[] Separators = { '\n', ',', '.', ' ', '\r', '\t' };

        internal static IEnumerable<HtmlNode> SelectNodesByPath(this HtmlNode node, string xPath)
        {
            return ((IEnumerable<HtmlNode>)node.SelectNodes(xPath) ?? new List<HtmlNode>()).ToList();
        }

        internal static bool ContainsAnyWord(this string passString, IEnumerable<string> checkList)
        {
            var words = passString.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
            return checkList.Any(x => words.Any(word => string.Equals(x, word, StringComparison.CurrentCultureIgnoreCase)));
        }

        internal static bool EndsWithAnyWord(this string passString, IEnumerable<string> checkList)
        {
            return checkList.Any(word => passString.ToLower().EndsWith(word.ToLower()));
        }

        internal static bool IsEqualToAnyWord(this string passString, IEnumerable<string> checkList)
        {
            return checkList.Any(word => string.Equals(word, passString, StringComparison.CurrentCultureIgnoreCase));
        }
    }
}
