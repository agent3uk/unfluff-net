﻿namespace Unfluff.Core
{
    public enum Language
    {
        English = 0,
        Arabic = 1,
        Czech = 2,
        Danish = 3,
        German = 4,
        Spanish = 5,
        Finnish = 6,
        French = 7,
        Hungarian = 8,
        Indonesian = 9,
        Italian = 10,
        Korean = 11,
        NorwegianBokmål = 12,
        Dutch = 13,
        Norwegian = 14,
        Polish = 15,
        Portuguese = 16,
        Russian = 17,
        Swedish = 18,
        Thai = 19,
        Turkish = 20,
        Chinese = 21
    }
}
