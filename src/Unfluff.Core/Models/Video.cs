﻿namespace Unfluff.Core.Models
{
    public class Video
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public string VideoUrl { get; set; }
    }
}