﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Unfluff.PageScraping;

namespace UnfluffNet.UnitTests
{
    [TestFixture]
    public class ScraperTestFixture
    {
        [TestCase("Case1","Expectation1")]
        [TestCase("Case2", "Expectation2")]
        [TestCase("Case3", "Expectation3")]
        [TestCase("Case4", "Expectation4")]
        [TestCase("Case5", "Expectation5")]
        [TestCase("Case10", "Expectation10")]
        public void Given_html_to_clean_result_body_should_match_expectation(string pageFileName, string expectationFileName)
        {
            var pageScraper = new PageScraper();
            var htmlStr = File.ReadAllText($"Resources/Pages/{pageFileName}.html");
            var expectation = File.ReadAllText($"Resources/Expectations/{expectationFileName}.txt");
            var acceptableScore = expectation.Length * 0.03;

            var results = pageScraper.Process(htmlStr, "http://www.example.com");

            var similarScore = LevenshteinDistance.Compute(expectation, results.Text);

            Console.WriteLine("Similarity score is " + similarScore + "; An acceptable score is " + acceptableScore);
            
            Assert.LessOrEqual(similarScore, acceptableScore);
        }

        [TestCase("Case4", "Expectation4")]
        public void Given_page_with_video_get_link_for_video(string pageFileName, string expectationFileName)
        {
            var pageScraper = new PageScraper();
            var htmlStr = File.ReadAllText($"Resources/Pages/{pageFileName}.html");

            var results = pageScraper.Process(htmlStr, "http://www.example.com");

            var videoFound = results.Videos.Count();

            Assert.AreEqual(3, videoFound);
        }
    }
}