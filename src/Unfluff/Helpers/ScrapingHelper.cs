﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using Unfluff.Extensions;
using Unfluff.Models;
using Unfluff.Utilities;

namespace Unfluff.Helpers
{
    internal class ScrapingHelper
    {
        private readonly StopWords stopWords;
        private readonly Formatter formatter;

        public ScrapingHelper(StopWords stopWords)
        {
            this.stopWords = stopWords;
            formatter = new Formatter(stopWords);
        }

        #region Helper methods
        // Keep track of # of 'texty' child nodes under this node with
        // graveityNodes attribute
        internal static void UpdateNodeCount(HtmlNode node, int addToCount)
        {
            var currentScore = 0;

            if (node.Attributes.Contains("gravityNodes"))
            {
                int.TryParse(node.GetAttributeValue("gravityNodes", string.Empty), out currentScore);
            }

            var newScore = currentScore + addToCount;

            if (node.Attributes.Contains("gravityNodes"))
            {
                node.Attributes["gravityNodes"].Value = Convert.ToString(newScore);
            }
            else
            {
                node.Attributes.Add("gravityNodes", Convert.ToString(newScore));
            }
        }

        // Return a node's gravity score (amount of texty-ness under it)
        internal static int GetScore(HtmlNode node)
        {
            var grvScoreString = node.Attributes["gravityScore"];
            if (grvScoreString == null)
            {
                return 0;
            }

            int retInt;
            int.TryParse(node.GetAttributeValue("gravityScore", "0"), out retInt);

            return retInt;
        }

        // Given a text node, check all previous siblings.
        // If the sibling node looks 'texty' and isn't too many
        // nodes away, it's probably some yummy text
        internal bool IsBoostable(HtmlNode node, Language lang)
        {
            var stepsAway = 0;
            const int minimumStopwordCount = 5;
            const int maxStepsawayFromNode = 3;
            var nodes = new List<HtmlNode>();
            var currentSibling = node;

            while (true)
            {
                if (currentSibling.PreviousSibling != null)
                {
                    nodes.Add(currentSibling.PreviousSibling);
                    currentSibling = currentSibling.PreviousSibling;
                }
                else
                {
                    break;
                }
            }

            var boostable = false;

            foreach (var currentNode in nodes.ToList())
            {
                // Make sure the node isn't more than 3 hops away
                if (currentNode.Name == "p")
                {
                    if (stepsAway >= maxStepsawayFromNode)
                    {
                        boostable = false;
                    }
                    else
                    {
                        var paraText = currentNode.InnerText;
                        var wordStats = stopWords.GetStopWords(paraText, lang);

                        // Check if the node contains more than 5 common words
                        if (wordStats.StopWordCount > minimumStopwordCount)
                        {
                            boostable = true;
                        }
                        else
                        {
                            stepsAway++;
                        }
                    }
                }
            }

            return boostable;
        }

        // Keep track of a node's score with a gravityScore attribute
        internal static void UpdateScore(HtmlNode node, double addToScore)
        {
            var currentScore = 0;
            var scoreString = node.GetAttributeValue("gravityScore", string.Empty);

            if (!string.IsNullOrEmpty(scoreString))
            {
                int.TryParse(scoreString, out currentScore);
            }

            var newScore = currentScore + addToScore;

            if (node.Attributes.Contains("gravityScore"))
            {
                node.Attributes["gravityScore"].Value = Convert.ToString(newScore, CultureInfo.InvariantCulture);
            }
            else
            {
                node.Attributes.Add("gravityScore", Convert.ToString(newScore, CultureInfo.InvariantCulture));
            }
        }

        internal static bool IsHighlinkDensity(HtmlNode node)
        {
            var links = node.Descendants("a").ToList();
            if (!links.Any())
            {
                return false;
            }

            var txt = node.InnerText;

            txt = txt.Replace('\n', ' ').Replace('\t', ' ').Replace('\r', ' ');
            txt = txt.SquishWhitespace();

            var words = txt.Split(new []{ " " }, StringSplitOptions.RemoveEmptyEntries);
            var numberOfWords = words.Length;

            var sb = links.Select(htmlNode => htmlNode.InnerText.Replace('\n', ' ').Replace('\t', ' ').Replace('\r', ' ').SquishWhitespace()).Where(t => !string.IsNullOrWhiteSpace(t)).ToList();

            var linkText = string.Join(" ", sb);
            var linkWords = linkText.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            var numberOfLinkWords = linkWords.Length;
            var numberOfLinks = links.Count;
            var percentLinkWords = (double)numberOfLinkWords / numberOfWords;
            var score = percentLinkWords * numberOfLinks;

            return score >= 1.0;
        }

        // Find the biggest chunk of text in the title
        internal static string BiggestTitleChunk(string title, char splitter)
        {
            var largeTextLength = 0;
            var largeTextIndex = 0;

            var titlePieces = title.Split(splitter).ToList();

            // find the largest substring
            foreach (var titlePiece in titlePieces)
            {
                if (titlePiece.Length > largeTextLength)
                {
                    largeTextLength = titlePiece.Length;
                    largeTextIndex = titlePieces.IndexOf(titlePiece);
                }
            }

            return titlePieces[largeTextIndex];
        }

        internal static Video GetObjectTag(HtmlNode node)
        {
            var srcNode = node.SelectNodesByPath("//param[@name=movie]").FirstOrDefault();
            if (srcNode == null)
            {
                return new Video();
            }

            var src = srcNode.GetAttributeValue("value", string.Empty);
            Video video = GetVideoAttrs(node);
            video.VideoUrl = src;

            return video;
        }

        internal static Video GetVideoAttrs(HtmlNode htmlNode)
        {
            var video = new Video
            {
                VideoUrl = htmlNode.GetAttributeValue("src", string.Empty),
                Height = htmlNode.GetAttributeValue("height", 0),
                Width = htmlNode.GetAttributeValue("width", 0)
            };

            return video;
        }

        // Remove any remaining trash nodes (clusters of nodes with little/no content)
        internal HtmlNode PostCleanup(HtmlNode targetNode, Language lang)
        {
            var node = AddSiblings(targetNode, lang);

            var children = node.ChildNodes.Where(child => child.Name != "p" && child.Name != "a" && !IsHeadingTag(child.Name)).ToList();

            foreach (var child in children)
            {
                if (IsHighlinkDensity(child) || IsTableAndNoParaExist(child) ||
                    !IsNodescoreThresholdMet(node, child))
                {
                    node.RemoveChild(child);
                }
            }

            return node;
        }

        private bool IsHeadingTag(string name) => name.StartsWith("h") 
                                                && name.Length == 2 
                                                && char.IsNumber(name.Last());


        internal HtmlNode AddSiblings(HtmlNode targetNode, Language lang)
        {
            var baselinescoreSiblingsPara = GetSiblingsScore(targetNode, lang);

            var sibs = new List<HtmlNode>();

            var currentSib = targetNode;
            do
            {
                if (currentSib.PreviousSibling != null)
                {
                    currentSib = currentSib.PreviousSibling;
                    sibs.Add(currentSib);
                }
                else
                {
                    currentSib = null;
                }
            } while (currentSib != null);

            foreach (var p in sibs.SelectMany(currentNode => GetSiblingsContent(lang, currentNode, baselinescoreSiblingsPara)))
            {
                targetNode.ChildNodes.Prepend(HtmlNode.CreateNode("<p>" + p + "</p>"));
            }

            return targetNode;
        }

        internal int GetSiblingsScore(HtmlNode topNode, Language lang)
        {
            var scoreBase = 100000;
            var paragraphsNumber = 0;
            var paragraphsScore = 0;
            // 25
            var nodesToCheck = topNode?.SelectNodesByPath(".//p").ToList() ?? new List<HtmlNode>();

            foreach (var node in nodesToCheck.Select(n => new
            {
                Text = n.InnerText,
                stopWords.GetStopWords(n.InnerText, lang).StopWordCount,
                IsHighlinkDensity = IsHighlinkDensity(n)
            }).Where(n => n.StopWordCount > 2 && !n.IsHighlinkDensity))
            {
                paragraphsNumber++;
                paragraphsScore += node.StopWordCount;
            }

            if (paragraphsNumber > 0)
            {
                scoreBase = paragraphsScore / paragraphsNumber;
            }

            return scoreBase;
        }

        internal static bool IsTableAndNoParaExist(HtmlNode e)
        {
            var subParagraphs = e.SelectNodesByPath(".//p").ToList();

            foreach (var p in subParagraphs.Where(n => n.InnerText.Length < 25).ToList())
            {
                p.Remove();
            }

            var subParagraphs2 = e.SelectNodesByPath(".//p").ToList();
            var excludeList = new List<string> { "td", "ul", "ol" };

            return !subParagraphs2.Any() && !excludeList.Contains(e.Name);
        }

        internal static bool IsNodescoreThresholdMet(HtmlNode node, HtmlNode e)
        {
            var topNodeScore = GetScore(node);
            var currentNodeScore = GetScore(e);
            var thresholdScore = topNodeScore * 0.08;

            var excludeList = new List<string> { "td", "ul", "ol", "blockquote" };
            
            return currentNodeScore >= thresholdScore || excludeList.Contains(e.Name);
        }

        internal IEnumerable<string> GetSiblingsContent(Language lang, HtmlNode currentSibling, int baselinescoreSiblingsPara)
        {
            if (currentSibling.Name == "p" && currentSibling.InnerText.Length > 0)
            {
                return new List<string> { currentSibling.InnerText };
            }

            var potentialParagraphs = currentSibling.SelectNodesByPath(".//p").Where(x => !string.IsNullOrEmpty(x.InnerText)).ToList();

            if (!potentialParagraphs.Any())
            {
                return new List<string>();
            }

            const double siblingBaselineScore = 0.30;
            var score = baselinescoreSiblingsPara * siblingBaselineScore;

            return potentialParagraphs
                .Select(p => new {
                    Text = p.InnerText, 
                    IsHighLinkDensity = IsHighlinkDensity(p),
                    Score = stopWords.GetStopWords(p.InnerText, lang).StopWordCount
                })
                .Where(p => p.Score > score && !p.IsHighLinkDensity)
                .Select(paragraph => paragraph.Text);
        }

        internal static string CleanText(string text)
        {
            text = Regex.Replace(text, @"[\r\n\t]", " ");
            text = Regex.Replace(text, @"\s\s+", " ");
            text = Regex.Replace(text, @"<!--.+?-->", string.Empty);
            text = Regex.Replace(text, @"�", string.Empty);

            return text.Trim();
        }
        #endregion

        #region PropertyMethods

        internal static string GetTitle(HtmlDocument document)
        {
            HtmlNode titleElement = document.DocumentNode.SelectSingleNode("//meta[@property='og:title']");
            string titleText = string.Empty;

            if (titleElement != null)
            {
                titleText = titleElement.GetAttributeValue("content", string.Empty);
            }

            if (string.IsNullOrEmpty(titleText))
            {
                titleElement = document.DocumentNode.SelectSingleNode("//title");
                titleText = titleElement?.InnerText;
            }

            if (titleElement == null)
            {
                return null;
            }

            var usedDelimeter = false;
            char[] strArray = { '|', '-', '»', ':' };
            foreach (var str in strArray)
            {
                if (titleText.Contains(str) && !usedDelimeter)
                {
                    titleText = BiggestTitleChunk(titleText, str);
                    usedDelimeter = true;
                }
            }

            return titleText.Trim();
        }

        internal static IEnumerable<string> GetKeywords(HtmlDocument document)
        {
            var keywords = string.Empty;
            var tag = document.DocumentNode.SelectSingleNode("//meta[@name='keywords']");

            if (tag != null)
            {
                keywords = tag.GetAttributeValue("content", string.Empty).Trim();
            }

            return keywords.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
        }

        internal static string GetCanonicalLink(HtmlDocument document, string articleUrl)
        {
            var links =
                document.DocumentNode.SelectNodesByPath("//link")
                    .Where(n => n.Attributes.Contains("rel") && n.GetAttributeValue("rel", string.Empty) == "canonical");

            var tag = links.FirstOrDefault();

            var canonicalLink = tag?.GetAttributeValue("href", string.Empty).Trim() ?? articleUrl;

            return Uri.IsWellFormedUriString(canonicalLink, UriKind.Absolute) ? canonicalLink : articleUrl;
        }

        internal static string GetFavicon(HtmlDocument document)
        {
            var favicon = string.Empty;

            var tag = document.DocumentNode.SelectNodesByPath("//link").FirstOrDefault(x => x.GetAttributeValue("rel", string.Empty).ToLower() == "shortcut icon");

            if (tag != null)
            {
                favicon = tag.GetAttributeValue("href", string.Empty).Trim();
            }

            return favicon;
        }

        internal static IEnumerable<string> GetTags(HtmlDocument document)
        {
            var documentNode = document.DocumentNode;

            var elements = documentNode.SelectNodesByPath("//a[@rel='tag']").ToList();

            if (!elements.Any())
            {
                elements = documentNode.SelectNodesByPath("//a[contains(@href,'/tag/')]")
                    .Union(documentNode.SelectNodesByPath("//a[contains(@href,'/tags/')]"))
                    .Union(documentNode.SelectNodesByPath("//a[contains(@href,'/topic/')]"))
                    .Union(documentNode.SelectNodesByPath("//a[contains(@href,'?keyword=')]")).ToList();

                if (!elements.Any())
                {
                    return new List<string>();
                }
            }

            var tags = new List<string>();

            foreach (var element in elements)
            {
                var tag = element.InnerText.Trim();
                tag = tag.Replace(@"\s", string.Empty).Replace(@"\t", string.Empty).Replace(@"\n", string.Empty);

                if (!string.IsNullOrEmpty(tag))
                {
                    tags.Add(tag);
                }
            }

            return tags.Distinct().ToList();
        }

        internal static string GetImage(HtmlDocument document)
        {
            var image = string.Empty;
            var documentNode = document.DocumentNode;

            var images = (documentNode.SelectNodesByPath("//meta[@property='og:image']"))
                .Union(documentNode.SelectNodesByPath("//meta[@itemprop=image]"))
                .Union(documentNode.SelectNodesByPath("//meta[@name='twitter:image:src']"))
                .Union(documentNode.SelectNodesByPath("//meta[@name='twitter:image']"))
                .Union(documentNode.SelectNodesByPath("//meta[@name='twitter:image0']")).ToList();

            if (images.Any())
            {
                image = images.First().GetAttributeValue("content", string.Empty);
            }

            return image;
        }

        internal static string GetLanguage(HtmlDocument document)
        {
            var lang = string.Empty;
            var documentNode = document.DocumentNode;

            var l = string.Empty;
            if (documentNode.Element("html") != null)
            {
                l = documentNode.Element("html").GetAttributeValue("lang", string.Empty);
            }

            if (string.IsNullOrEmpty(l))
            {
                var tag =
                    documentNode.SelectNodesByPath("//meta[@name=lang]")
                        .Union(documentNode.SelectNodesByPath("//meta[@http-equiv=content-language]"))
                        .ToList();

                if (tag.Any()) l = tag.First().GetAttributeValue("content", string.Empty);
            }

            if (!string.IsNullOrEmpty(l))
            {
                var value = l.Substring(0, l.Length > 2 ? 2 : l.Length);
                if (Regex.IsMatch("value", @"^[A-Za-z]{2}$"))
                {
                    lang = value.ToLower();
                }
            }

            return string.IsNullOrEmpty(lang) ? "en" : lang;
        }

        internal static string GetDescription(HtmlDocument document)
        {
            var description = string.Empty;
            var documentNode = document.DocumentNode;

            if (string.IsNullOrEmpty(description))
            {
                var tag = documentNode.SelectSingleNode("//meta[@name='description']") ??
                          documentNode.SelectSingleNode("//meta[@property='og:description']");

                if (tag != null)
                {
                    description = tag.GetAttributeValue("content", string.Empty).Trim();
                }
            }

            return description;
        }

        internal string GetText(HtmlNode topNode, Language lang)
        {
            var text = string.Empty;

            if (string.IsNullOrEmpty(text))
            {
                if (topNode != null)
                {
                    topNode = PostCleanup(topNode, lang);
                    text = formatter.FormatString(topNode, lang);
                }
                else
                {
                    text = string.Empty;
                }
            }

            return text;
        }

        internal HtmlNode GetTopNode(HtmlDocument document, Language lang)
        {
            HtmlNode topNode = null;
            var doc = document.DocumentNode;

            // Walk all the p, pre and td nodes
            var nodesToCheck = doc.SelectNodesByPath(".//p | .//pre | .//td").ToList();

            var startingBoost = 1.0;
            var cnt = 0;
            var i = 0;
            var parentNodes = new List<HtmlNode>();

            // If a node contains multiple common words and isn't just a bunch
            // of links, it's worth consideration of being 'texty'
            var nodesWithText = nodesToCheck.Where(node => stopWords.GetStopWords(node.InnerText, lang).StopWordCount > 2
                                                           && !IsHighlinkDensity(node)).ToList();

            var nodesNumbers = nodesWithText.Count;
            const int negativeScoring = 0;
            var bottomNegativescoreNodes = nodesNumbers * 0.25;

            // Walk all the potentially 'texty' nodes
            foreach (var node in nodesWithText)
            {
                var boostScore = 0.0;

                // If this node has nearby nodes that contain
                // some good text, give the node some boost points
                if (IsBoostable(node, lang))
                {
                    if (cnt >= 0)
                    {
                        boostScore = (1.0 / startingBoost) * 50;
                        startingBoost++;
                    }
                }

                if (nodesNumbers > 15)
                {
                    if ((nodesNumbers - i) <= bottomNegativescoreNodes)
                    {
                        var booster = bottomNegativescoreNodes - (nodesNumbers - i);
                        boostScore = -1 * Math.Pow(booster, 2);
                        var negScore = Math.Abs(boostScore) + negativeScoring;

                        if (negScore > 40)
                        {
                            boostScore = 5.0;
                        }
                    }
                }

                // Give the current node a score of how many common words
                // it contains plus any boost
                var upscore = Math.Floor(stopWords.GetStopWords(node.InnerText, lang).StopWordCount + boostScore);

                //Propigate the score upwards
                var parentNode = node.ParentNode;
                UpdateScore(parentNode, upscore);
                UpdateNodeCount(parentNode, 1);

                if (!parentNodes.Contains(parentNode))
                {
                    parentNodes.Add(parentNode);
                }

                var grandParentNode = parentNode.ParentNode;

                if (grandParentNode != null)
                {
                    UpdateNodeCount(grandParentNode, 1);
                    UpdateScore(grandParentNode, upscore / 2);

                    if (!parentNodes.Contains(grandParentNode))
                    {
                        parentNodes.Add(grandParentNode);
                    }
                }

                cnt++;
                i++;
            }

            var topNodeScore = 0;

            // Walk each parent and parent-parent and find the one that
            // contains the highest sum score of 'texty' child nodes.
            // That's probably out best node!
            foreach (var parentNode in parentNodes)
            {
                var score = GetScore(parentNode);

                if (score > topNodeScore)
                {
                    topNode = parentNode;
                    topNodeScore = score;
                }

                if (topNode == null)
                {
                    topNode = parentNode;
                }
            }

            return topNode;
        }

        internal static IEnumerable<Video> GetVideos(HtmlNode topNode)
        {
            if (topNode == null) return new List<Video>();

            var candidates = topNode.SelectNodesByPath(".//iframe | .//embed | .//object | .//video").ToList();

            var videoList = new List<Video>();

            foreach (var candidate in candidates)
            {
                var tag = candidate.Name;

                if (tag == "embed")
                {
                    if (candidate.ParentNode != null && candidate.ParentNode.Name == "object")
                    {
                        videoList.Add(GetObjectTag(candidate));
                    }
                    else
                    {
                        videoList.Add(GetVideoAttrs(candidate));
                    }
                }
                else if (tag == "object")
                {
                    videoList.Add(GetObjectTag(candidate));
                }
                else if (tag == "iframe" || tag == "video")
                {
                    videoList.Add(GetVideoAttrs(candidate));
                }
            }

            // Filter out junky or duplicate videos
            var urls = new List<string>();
            var results = new List<Video>();

            foreach (var vid in videoList.Where(vid => vid.Height > 0 && vid.Width > 0 && !urls.Contains(vid.VideoUrl)))
            {
                results.Add(vid);
                urls.Add(vid.VideoUrl);
            }

            return results;
        }

        internal static string GetAuthor(HtmlDocument document)
        {
            var author = string.Empty;
            var documentNode = document.DocumentNode;

            var authorCandidates =
                documentNode.SelectNodesByPath("//meta[@property='article:author']")
                    .Union(documentNode.SelectNodesByPath("//meta[@property='og:article:author']"))
                    .Union(documentNode.SelectNodesByPath("//meta[@name='author']"))
                    .Union(documentNode.SelectNodesByPath("//meta[@name='dcterms.creator']"))
                    .Union(documentNode.SelectNodesByPath("//meta[@name='DC.creator']"))
                    .Union(documentNode.SelectNodesByPath("//meta[@name='DC.Creator']"))
                    .Union(documentNode.SelectNodesByPath("//meta[@name='dc.creator']"))
                    .Union(documentNode.SelectNodesByPath("//meta[@name='DC.creator']"))
                    .Union(documentNode.SelectNodesByPath("//meta[@name='creator']"))
                    .Select(n => n.InnerText).Where(a => !string.IsNullOrEmpty(a))
                    .ToList();

            if (!authorCandidates.Any())
            {
                authorCandidates =
                    documentNode.SelectNodesByPath("//span[contains(@class, 'author')]")
                        .Union(documentNode.SelectNodesByPath("//p[contains(@class, 'author')]"))
                        .Union(documentNode.SelectNodesByPath("//div[contains(@class, 'author')]"))
                        .Union(documentNode.SelectNodesByPath("//span[contains(@class, 'byline')]"))
                        .Union(documentNode.SelectNodesByPath("//p[contains(@class, 'byline')]"))
                        .Union(documentNode.SelectNodesByPath("//div[contains(@class, 'byline')]"))
                        .Select(n => n.InnerText)
                        .Where(a => !string.IsNullOrEmpty(a))
                        .ToList();
            }

            var authorList = new List<string>();

            foreach (var candidate in authorCandidates)
            {
                var tempAuthor = candidate.Replace("Written by ", "").Replace("By ", "").Replace("Author: ", "").Trim();
                if (!string.IsNullOrEmpty(tempAuthor))
                {
                    authorList.Add(CleanText(tempAuthor));
                }
            }

            if (authorList.Any())
            {
                author = authorList.GroupBy(a => a).OrderByDescending(g => g.Count()).First().Key;
            }

            return author;
        }

        internal static string GetCopyright(HtmlDocument document)
        {
            var documentNode = document.DocumentNode;

            var copyrightCandidates =
                documentNode.SelectNodesByPath("//p[contains(@class, 'copyright')]")
                    .Union(documentNode.SelectNodesByPath("//div[contains(@class, 'copyright')]"))
                    .Union(documentNode.SelectNodesByPath("//span[contains(@class, 'copyright')]"))
                    .Union(documentNode.SelectNodesByPath("//li[contains(@class, 'copyright')]"))
                    .Union(documentNode.SelectNodesByPath("//p[contains(@id, 'copyright')]"))
                    .Union(documentNode.SelectNodesByPath("//div[contains(@id, 'copyright')]"))
                    .Union(documentNode.SelectNodesByPath("//span[contains(@id, 'copyright')]"))
                    .Union(documentNode.SelectNodesByPath("//li[contains(@id, 'copyright')]"))
                    .Select(n => n.InnerText).Where(a => !string.IsNullOrEmpty(a))
                    .ToList();

            string text = copyrightCandidates.FirstOrDefault();

            if (string.IsNullOrEmpty(text))
            {
                text = documentNode?.SelectSingleNode("//body")?.InnerHtml ?? string.Empty;
                text = Regex.Replace(text, @"\s*[\r\n]+\s*", ". "); 

                if (!text.Contains("©"))
                {
                    return string.Empty;
                }
            }

            string copyright = "";

            var symbolIndex = text.IndexOf('©');
            if (symbolIndex > -1)
            {
                text = text.Remove(0, symbolIndex + 1);

                var copyrightIndex = text.IndexOf("copyright", StringComparison.CurrentCultureIgnoreCase);
                if (copyrightIndex > -1)
                {
                    text = text.Remove(0, copyrightIndex + "copyright".Length + 1);

                    copyright = "copyright " + text.Trim();
                }
            }

            return CleanText(copyright);
        }

        internal static DateTime GetPublishedDate(HtmlDocument document)
        {
            var documentNode = document.DocumentNode;

            var dateCandidates =
                documentNode.SelectNodesByPath("//meta").Where(e =>
                    e.GetAttributeValue("property", "") == "article:published_time")
                    .Union(documentNode.SelectNodesByPath("//meta[contains(@itemprop,'datePublished')]"))
                    .Union(documentNode.SelectNodesByPath("//meta[@name='dcterms.modified']"))
                    .Union(documentNode.SelectNodesByPath("//meta[@name='dcterms.date']"))
                    .Union(documentNode.SelectNodesByPath("//meta[@name='DC.date.issued']"))
                    .Union(documentNode.SelectNodesByPath("//meta[@name='dc.date.issued']"))
                    .Union(documentNode.SelectNodesByPath("//meta[@name='dc.date.modified']"))
                    .Union(documentNode.SelectNodesByPath("//meta[@name='dc.date.created']"))
                    .Union(documentNode.SelectNodesByPath("//meta[@name='DC.date']"))
                    .Union(documentNode.SelectNodesByPath("//meta[@name='DC.Date']"))
                    .Union(documentNode.SelectNodesByPath("//meta[@name='dc.date']"))
                    .Union(documentNode.SelectNodesByPath("//meta[@name='date']"))
                    .Union(documentNode.SelectNodesByPath("//time[contains(@itemprop,'pubDate')]"))
                    .Union(documentNode.SelectNodesByPath("//time[contains(@itemprop,'pubdate')]"))
                    .Union(documentNode.SelectNodesByPath("//span[contains(@itemprop,'datePublished')]"))
                    .Union(documentNode.SelectNodesByPath("//span[contains(@property,'datePublished')]"))
                    .Union(documentNode.SelectNodesByPath("//p[contains(@itemprop,'datePublished')]"))
                    .Union(documentNode.SelectNodesByPath("//p[contains(@property,'datePublished')]"))
                    .Union(documentNode.SelectNodesByPath("//div[contains(@itemprop,'datePublished')]"))
                    .Union(documentNode.SelectNodesByPath("//div[contains(@property,'datePublished')]"))
                    .Union(documentNode.SelectNodesByPath("//li[contains(@itemprop,'datePublished')]"))
                    .Union(documentNode.SelectNodesByPath("//li[contains(@property,'datePublished')]"))
                    .Union(documentNode.SelectNodesByPath("//span[contains(@itemprop,'datePublished')]"))
                    .Union(documentNode.SelectNodesByPath("//time"))
                    .Union(documentNode.SelectNodesByPath("//span[contains(@class,'date')]"))
                    .Union(documentNode.SelectNodesByPath("//p[contains(@class,'date')]"))
                    .Union(documentNode.SelectNodesByPath("//div[contains(@class,'date')]"))
                    .Select(n =>
                        n.GetAttributeValue("content", "") != "" ?
                            n.GetAttributeValue("content", "")
                            : (
                                n.GetAttributeValue("datetime", "") != ""
                                    ? n.GetAttributeValue("datetime", "")
                                    : CleanText(n.InnerText)
                                ))
                    .Where(a => !string.IsNullOrEmpty(a))
                    .ToList();

            return GetFirstValidDateTime(dateCandidates);
        }

        private static DateTime GetFirstValidDateTime(IEnumerable<string> candidates)
        {
            var returnDateTime = DateTime.UtcNow;

            foreach (var candidate in candidates)
            {
                if (DateTime.TryParse(candidate, out returnDateTime))
                {
                    break;
                }
            }

            return returnDateTime;
        }

        internal static string GetPublisher(HtmlDocument document)
        {
            var documentNode = document.DocumentNode;

            var publisherCandidates =
                documentNode.SelectNodesByPath("//meta")
                    .Where(e => string.Equals(e.GetAttributeValue("property", ""), @"og:site_name", StringComparison.CurrentCultureIgnoreCase))
                    .Union(documentNode.SelectNodesByPath("//meta[@name='dc.publisher']"))
                    .Union(documentNode.SelectNodesByPath("//meta[@name='DC.publisher']"))
                    .Union(documentNode.SelectNodesByPath("//meta[@name='DC.Publisher']"))
                    .Select(n =>
                        n.GetAttributeValue("content", "")).Where(p => !string.IsNullOrEmpty(p)).ToList();

            return (publisherCandidates.FirstOrDefault() ?? string.Empty).Trim();
        }

        internal IDictionary<string, string> GetLinks(HtmlNode topNode, Language language, string articleUrl)
        {
            if (topNode == null) return new Dictionary<string, string>();
            topNode = PostCleanup(topNode, language);
            var links = new Dictionary<string, string>();
            var nodes = topNode.SelectNodesByPath("//a");

            var baseUri = new Uri(articleUrl, UriKind.Absolute);

            foreach (var node in nodes.Where(n => n != null))
            {
                var href = node.GetAttributeValue("href", string.Empty);
                var text = node.InnerText;

                if (!string.IsNullOrEmpty(href) && !string.IsNullOrEmpty(text))
                {
                    bool valid = Uri.IsWellFormedUriString(href, UriKind.RelativeOrAbsolute);

                    if (valid)
                    {
                        var path = new Uri(href, UriKind.RelativeOrAbsolute);

                        if (!path.IsAbsoluteUri)
                        {
                            var uriStr = baseUri.Scheme + "://" + baseUri.Host + href;
                            valid = Uri.IsWellFormedUriString(uriStr, UriKind.Absolute);
                            if (valid) path = new Uri(uriStr);
                        }

                        if (valid && !links.ContainsKey(path.AbsoluteUri))
                        {
                            links.Add(path.AbsoluteUri, text);
                        }
                    }
                }
            }

            return links;
        }

        #endregion
    }
}
