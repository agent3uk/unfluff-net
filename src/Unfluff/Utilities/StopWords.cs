﻿using System.Collections.Generic;
using System.Linq;

namespace Unfluff.Utilities
{
    internal class StopWords
    {
        private readonly Dictionary<int, List<string>> cache = new Dictionary<int, List<string>>();      

        private static string GetResource(Language language)
        {
            switch (language)
            {
                case Language.English:
                    return Resources.stopwords_en;
                case Language.German:
                    return Resources.stopwords_de;
                case Language.Spanish:
                    return Resources.stopwords_es;
                case Language.Arabic:
                    return Resources.stopwords_ar;
                case Language.Czech:
                    return Resources.stopwords_cs;
                case Language.Danish:
                    return Resources.stopwords_da;
                case Language.Finnish:
                    return Resources.stopwords_fi;
                case Language.French:
                    return Resources.stopwords_fr;
                case Language.Hungarian:
                    return Resources.stopwords_hu;
                case Language.Indonesian:
                    return Resources.stopwords_id;
                case Language.Italian:
                    return Resources.stopwords_it;
                case Language.Korean:
                    return Resources.stopwords_ko;
                case Language.NorwegianBokmål:
                    return Resources.stopwords_nb;
                case Language.Dutch:
                    return Resources.stopwords_nl;
                case Language.Norwegian:
                    return Resources.stopwords_no;
                case Language.Polish:
                    return Resources.stopwords_pl;
                case Language.Portuguese:
                    return Resources.stopwords_pt;
                case Language.Russian:
                    return Resources.stopwords_ru;
                case Language.Swedish:
                    return Resources.stopwords_sv;
                case Language.Thai:
                    return Resources.stopwords_th;
                case Language.Turkish:
                    return Resources.stopwords_tr;
                case Language.Chinese:
                    return Resources.stopwords_zh;
                default:
                    return string.Empty;
            }
        }

        public StopWordsObj GetStopWords(string content, Language language)
        {
            List<string> stopWords;
            var cacheKey = (int) language;
            
            if (cache.ContainsKey(cacheKey))
            {
                stopWords = cache[cacheKey];
            }
            else
            {
                var resource = GetResource(language);

                stopWords = resource.Replace("\r", "").Replace("\n", ";").Split(';').ToList();
                cache.Add(cacheKey, stopWords);
            }

            var strippedInput = RemovePunctuation(content);
            var words = CandidateWords(strippedInput);
            var overlappingStopwords = new List<string>();
            var count = 0;

            foreach (var word in words)
            {
                count++;
                if (stopWords.Contains(word.ToLower()))
                {
                    overlappingStopwords.Add(word.ToLower());
                }
            }

            return StopWordsObj.Create(count, overlappingStopwords.Count, overlappingStopwords);
        }

        public class StopWordsObj
        {
            private StopWordsObj() { }

            public int WordCount { get; set; }
            public int StopWordCount { get; set; }
            public List<string> StopWords { get; set; }

            public static StopWordsObj Create(int wordCount, int stopWordCount, IEnumerable<string> stopWords)
                => new StopWordsObj
                {
                    WordCount = wordCount,
                    StopWords = stopWords.ToList(),
                    StopWordCount = stopWordCount
                };
        }

        private static IEnumerable<string> CandidateWords(string strippedInput)
            => strippedInput.Split(' ');

        private static string RemovePunctuation(string content)
            => new string(content.Where(c => !char.IsPunctuation(c)).ToArray());
    }
}
