﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using HtmlAgilityPack;
using Unfluff.Extensions;

namespace Unfluff.Utilities
{
    internal class Formatter
    {
        private readonly StopWords stopWords;

        public Formatter(StopWords stopWords)
        {
            this.stopWords = stopWords;
        }

        internal string FormatString(HtmlNode topNode, Language language)
        {
            RemoveNegativescoresNodes(topNode);
            LinksToText(topNode);
            AddNewlineToBr(topNode);
            ReplaceWithText(topNode);
            RemoveFewwordsParagraphs(topNode, language);

            return ConvertToText(topNode);
        }

        private static string ConvertToText(HtmlNode topNode)
        {
            var txts = new List<string>();
            var nodes = topNode.ChildNodes;
            StringBuilder hangingText = new StringBuilder();
            string txt;

            foreach (var node in nodes)
            {
                var nodeType = node.NodeType;
                var nodeName = node.Name;

                if (nodeType == HtmlNodeType.Text)
                {
                    hangingText.Append(node.InnerText);
                }
                else if (nodeName == "ul")
                {
                    hangingText.Append(UlToText(node));
                }

                if (hangingText.Length > 0)
                {
                    txt = CleanParagraphText(hangingText.ToString());
                    txts = txts.Union(txt.Split('\n', '\r')).ToList();
                    hangingText.Clear();
                }
                else
                {
                    txt = CleanParagraphText(node.InnerText);
                    txts = txts.Union(txt.Split('\n', '\r')).ToList();
                }
            }

            if (hangingText.Length > 0)
            {
                txt = CleanParagraphText(hangingText.ToString());
                txts = txts.Union(txt.Split('\n', '\r')).ToList();
            }

            txts = txts.Where(x => x.Any(c => char.IsLetter(c) || char.IsLetter(c))).ToList();

            var sb = new StringBuilder();
            foreach (var t in txts)
            {
                var breakSplits = t.Split(new[] {@"\r\n"}, StringSplitOptions.RemoveEmptyEntries).ToList();

                foreach (var s in breakSplits)
                {
                    sb.AppendLine(s.Trim());

                    var isTheEnd = txts.IndexOf(t) == txts.Count - 1 && breakSplits.IndexOf(s) == breakSplits.Count - 1;

                    if (!isTheEnd) sb.AppendLine();
                }
            }

            return sb.ToString();
        }

        private static string UlToText(HtmlNode node)
        {
            var nodes = node.Descendants("li");
            var txt = nodes.Aggregate(string.Empty, (current, htmlNode) => current + ("\n * " + htmlNode.InnerText));

            return txt + "\n";
        }

        private static string CleanParagraphText(string rawText)
        {
            var txt = rawText.Trim();
            txt = WebUtility.HtmlDecode(txt);
            txt = txt.SquishWhitespace();
            txt = txt.Replace(@"\s\t", " ");
            return txt;
        }

        private void RemoveFewwordsParagraphs(HtmlNode topNode, Language language)
        {
            var allNodes = topNode.Descendants().ToList();

            foreach (var node in allNodes)
            {
                var words = stopWords.GetStopWords(node.InnerText, language);

                if (
                    (node.Name != "br" || node.InnerText != @"\r")
                    && words.WordCount == 0
                    && !node.SelectNodesByPath("//object").Any()
                    && !node.SelectNodesByPath("//embed").Any()
                    )
                {
                    node.Remove();
                }
                else
                {
                    var trimmed = node.InnerText.Trim();
                    if (!string.IsNullOrWhiteSpace(trimmed))
                    {
                        if (trimmed[0] == '(' && trimmed[trimmed.Length - 1] == ')')
                        {
                            node.Remove();
                        }
                    }
                }
            }
        }

        private static void ReplaceWithText(HtmlNode topNode)
        {
            var nodes = topNode.SelectNodesByPath("//b")
                .Union(topNode.SelectNodesByPath("//strong"))
                .Union(topNode.SelectNodesByPath("//i"))
                .Union(topNode.SelectNodesByPath("//br"))
                .Union(topNode.SelectNodesByPath("//sup")).ToList();

            foreach (var htmlNode in nodes)
            {
                htmlNode.ParentNode.ReplaceChild(HtmlNode.CreateNode(htmlNode.InnerText), htmlNode);
            }
        }

        private static void AddNewlineToBr(HtmlNode topNode)
        {
            var brs = topNode.SelectNodesByPath("//br");

            foreach (var htmlNode in brs)
            {
                htmlNode.ParentNode.RemoveChild(htmlNode);
            }
        }

        private static void LinksToText(HtmlNode topNode)
        {
            var nodes = topNode.SelectNodesByPath("//a");

            foreach (var htmlNode in nodes)
            {
                htmlNode.ParentNode.ReplaceChild(HtmlNode.CreateNode(htmlNode.InnerText), htmlNode);
            }
        }

        private static void RemoveNegativescoresNodes(HtmlNode topNode)
        {
            var gravityItems =
                topNode.Descendants().Where(node => node.Attributes?.AttributesWithName("gravityScore").Any() == true).ToList();

            foreach (var gravityItem in gravityItems)
            {
                var gravScore = gravityItem.GetAttributeValue("gravityScore", "0");
                decimal score;
                var parsed = decimal.TryParse(gravScore, out score);

                if (!parsed || score < 1)
                {
                    gravityItem.Remove();
                }
            }
        }
    }
}
