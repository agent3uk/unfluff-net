﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using Unfluff.Extensions;

namespace Unfluff.Utilities
{
    internal class Cleaner
    {
        internal static void CleanArticle(HtmlNode doc)
        {
            RemoveBodyClasses(doc);
            CleanArticleTags(doc);
            CleanEmTags(doc);
            CleanCodeBlocks(doc);
            RemoveDropCaps(doc);
            RemoveScriptsStylesComments(doc);
            CleanBadTags(doc);
            RemoveNodesRegex(doc, "/^caption$/");
            RemoveNodesRegex(doc, "/ google /");
            RemoveNodesRegex(doc, "/^[^entry-]more.*$/");
            RemoveNodesRegex(doc, "/[^-]facebook/");
            RemoveNodesRegex(doc, "/facebook-broadcasting/");
            RemoveNodesRegex(doc, "/[^-]twitter/");
            CleanParaSpans(doc);
            CleanUnderlines(doc);
            CleanErrantLinebreaks(doc);
            DivToPara(doc, "div");
            DivToPara(doc, "span");
        }

        private static void RemoveBodyClasses(HtmlNode doc)
        {
            var body = doc.Descendants().FirstOrDefault(n => n.Name == "body");
            body?.Attributes["class"]?.Remove();
        }

        private static void CleanArticleTags(HtmlNode doc)
        {
            var articles = doc.Descendants("article").ToList();
            foreach (var node in articles)
            {
                node.Attributes.Remove("id");
                node.Attributes.Remove("name");
                node.Attributes.Remove("class");
            }
        }

        private static void CleanEmTags(HtmlNode doc)
        {
            var ems = doc.Descendants("em").ToList();

            foreach (var node in ems.Where(node => !node.Descendants("img").Any()))
            {
                if (!string.IsNullOrEmpty(node.InnerText))
                {
                    string innerHtml = node.InnerHtml ?? string.Empty;

                    node.ParentNode.ReplaceChild(HtmlNode.CreateNode(innerHtml), node);
                }
            }
        }

        private static void CleanCodeBlocks(HtmlNode doc)
        {
            string[] codeBlocks = { "pre code", "code", "pre" };
            var nodes = doc.Descendants().Where(n =>
                codeBlocks.Contains(n.Name) ||
                (n.Name == "ul" && n.GetAttributeValue("class", string.Empty).Contains("task-list")) ||
                n.GetAttributeValue("class", string.Empty).Contains("highlight-")
                ).ToList();

            foreach (var node in nodes)
            {
                node.ParentNode.ReplaceChild(HtmlNode.CreateNode(node.InnerText), node);
            }
        }

        private static void RemoveDropCaps(HtmlNode doc)
        {
            var nodes =
                doc.Descendants()
                    .Where(
                        n =>
                            n.ParentNode != null && n.ParentNode.Name == "span" &&
                            (n.GetAttributeValue("class", string.Empty).Contains("dropcap") ||
                             n.GetAttributeValue("class", string.Empty).Contains("drop_cap"))).ToList();

            foreach (var node in nodes)
            {
                node.ParentNode.ReplaceChild(HtmlNode.CreateNode(node.InnerHtml), node);
            }
        }

        private static void RemoveScriptsStylesComments(HtmlNode doc)
        {
            var nodes = doc.Descendants().Where(n => n.Name == "script" || n.Name == "style" || n.NodeType == HtmlNodeType.Comment).ToList();
            foreach (var node in nodes)
            {
                node.Remove();
            }
        }

        private static void CleanBadTags(HtmlNode doc)
        {
            var containsWords = new List<string> { "combx", "retweet", "mediaarticlerelated", "menucontainer", "navbar", "nav", "related", "promo", "advert", "menu", "partner-gravity-ad", "partner-gravity-ad", "video-full-transcript", "storytopbar-bucket", "utility-bar", "inline-share-tools", "comment", "PopularQuestions", "contact", "foot", "footer", "Footer", "footnote", "cnn_strycaptiontxt", "cnn_html_slideshow", "cnn_strylftcntnt", "link", "button", "links", "shoutbox", "sponsor", "tags", "socialnetworking", "socialNetworking", "cnnStryHghLght", "cnn_stryspcvbx", "pagetools", "post-attributes", "welcome_form", "contentTools2", "the_answers", "communitypromo", "runaroundLeft", "subscribe", "vcard", "articleheadings", "date", "popup", "author-dropdown", "tools", "socialtools", "byline", "konafilter", "KonaFilter", "breadcrumbs", "wp-caption-text", "legende", "ajoutVideo", "timestamp", "js_replies", "facebook", "twitter", "tweet", "googleplus", "google", "cookie", "menu", "cookies", "sidemenu", "ad", "notification", "notifications", "notify", "meta" };
            var endsWithWords = new List<string> { "meta" };
            var isWords = new List<string> { "side", "fn", "print", "inset" };

            var toRemove =
                doc.Descendants()
                    .Where(
                        n => CheckNodeForBadWords(n, containsWords, endsWithWords, isWords)
                    ).ToList();

            foreach (var node in toRemove)
            {
                node.Remove();
            }
        }

        private static bool CheckNodeForBadWords(HtmlNode node, List<string> containsWords, List<string> endsWithWords, List<string> isWords)
        {
            if (node.Attributes.Contains("class"))
            {
                if (containsWords.Any(word => node.GetAttributeValue("class", string.Empty).ToLower().Contains(word.ToLower())))
                {
                    return true;
                }

                if (node.GetAttributeValue("class", string.Empty).EndsWithAnyWord(endsWithWords))
                {
                    return true;
                }

                if (node.GetAttributeValue("class", string.Empty).IsEqualToAnyWord(isWords))
                {
                    return true;
                }
            }

            if (node.Attributes.Contains("name"))
            {
                if (containsWords.Any(word => node.GetAttributeValue("name", string.Empty).ToLower().Contains(word.ToLower())))
                {
                    return true;
                }

                if (node.GetAttributeValue("name", string.Empty).EndsWithAnyWord(endsWithWords))
                {
                    return true;
                }

                if (node.GetAttributeValue("name", string.Empty).IsEqualToAnyWord(isWords))
                {
                    return true;
                }
            }

            if (node.Attributes.Contains("id"))
            {
                if (containsWords.Any(word => node.GetAttributeValue("id", string.Empty).ToLower().Contains(word.ToLower())))
                {
                    return true;
                }

                if (node.GetAttributeValue("id", string.Empty).EndsWithAnyWord(endsWithWords))
                {
                    return true;
                }

                if (node.GetAttributeValue("id", string.Empty).IsEqualToAnyWord(isWords))
                {
                    return true;
                }
            }

            return false;
        }

        private static void RemoveNodesRegex(HtmlNode doc, string pattern)
        {
            var re = new Regex(pattern);
            var nodes =
                doc.Descendants()
                    .Where(
                        n =>
                            n.Name == "div" &&
                            (re.IsMatch(n.GetAttributeValue("id", string.Empty)) ||
                             re.IsMatch(n.GetAttributeValue("class", string.Empty)))
                           ).ToList();

            foreach (var node in nodes)
            {
                node.Remove();
            }
        }

        private static void CleanParaSpans(HtmlNode doc)
        {
            var nodes =
                doc.Descendants()
                    .Where(n => n.Name == "span" && n.ParentNode != null && n.ParentNode.Name == "p").ToList();

            foreach (var node in nodes)
            {
                node.ParentNode.ReplaceChild(HtmlNode.CreateNode(node.InnerHtml), node);
            }
        }

        private static void CleanUnderlines(HtmlNode doc)
        {
            var nodes = doc.Descendants("u").ToList();

            foreach (var node in nodes)
            {
                node.ParentNode.ReplaceChild(HtmlNode.CreateNode(node.InnerHtml), node);
            }
        }

        // For plain text nodes directly inside of p tags that contain random single
        // line breaks, remove those junky line breaks. They would never be rendered
        // by a browser anyway.
        private static void CleanErrantLinebreaks(HtmlNode doc)
        {
            var nodes = doc.Descendants("p").ToList();

            foreach (var node in nodes)
            {
                foreach (var c in node.ChildNodes.Where(n => n.Name == "text").ToList())
                {
                    var newMarkup = Regex.Replace(c.InnerText, "([^\n])\n([^\n])", "$1 $2");
                    c.ParentNode.ReplaceChild(HtmlNode.CreateNode(newMarkup), c);
                }
            }
        }

        private static void DivToPara(HtmlNode doc, string domType)
        {
            var divs = doc.Descendants().Where(n => n.Name == domType).ToList();
            string[] tags = { "a", "blockquote", "dl", "div", "img", "ol", "p", "pre", "table", "ul" };

            //Foreach div or span on page
            foreach (var div in divs)
            {
                // Find elements of certain types in the div or span
                var items = div.Descendants().Where(n => tags.Contains(n.Name)).ToList();

                if (!items.Any())
                {
                    // If no elements then change div/span to p
                    ReplaceWithPara(div);
                }
                else
                {
                    // Get the content of all the elements that will be replaced
                    var replaceNodes = GetReplacementNodes(div);

                    // Replace child nodes with para's
                    foreach (var node in replaceNodes.Where(node => !string.IsNullOrWhiteSpace(node)))
                    {
                        div.ParentNode?.InsertBefore(HtmlNode.CreateNode("<p>" + node + "</p>"), div);
                    }

                    div.Remove();
                }
            }
        }

        private static IEnumerable<string> GetReplacementNodes(HtmlNode div)
        {
            var replacementText = new List<string>();
            var nodesToReturn = new List<string>();
            var nodesToRemove = new List<HtmlNode>();
            var children = div.ChildNodes;

            foreach (var child in children)
            {
                // node is a p
                // and already have some replacement text
                if (child.Name == "p" && replacementText.Count > 0)
                {
                    var txt = string.Join("", replacementText);
                    nodesToReturn.Add(txt);
                    replacementText = new List<string>();
                    nodesToReturn.Add(child.InnerHtml);
                }
                // node is a text node
                else if (child.NodeType == HtmlNodeType.Text)
                {
                    var kidTextNode = child;
                    var kidText = child.InnerText;
                    var replaceText = Regex.Replace(kidText.Replace(@"\n", string.Empty).Replace(@"\t", string.Empty), @"^\s+$",
                        string.Empty);

                    if (replaceText.Length > 1)
                    {
                        var previousSiblingNode = kidTextNode.PreviousSibling;

                        while (previousSiblingNode != null && previousSiblingNode.Name == "a" &&
                               previousSiblingNode.GetAttributeValue("grv-usedalready", string.Empty) != "yes")
                        {
                            var outer = " " + previousSiblingNode.InnerHtml + " ";
                            replacementText.Add(outer);
                            nodesToRemove.Add(previousSiblingNode);
                            if (previousSiblingNode.Attributes.Any(n => n.Name == "grv-usedalready"))
                            {
                                previousSiblingNode.Attributes["grv-usedalready"].Value = "yes";
                            }
                            else
                            {
                                previousSiblingNode.Attributes.Add("grv-usedalready", "yes");
                            }

                            previousSiblingNode = previousSiblingNode.PreviousSibling;
                        }

                        replacementText.Add(replaceText);

                        var nextSiblingNode = kidTextNode.NextSibling;

                        while (nextSiblingNode != null && nextSiblingNode.Name == "a" &&
                               nextSiblingNode.GetAttributeValue("grv-usedalready", string.Empty) != "yes")
                        {
                            var outer = " " + nextSiblingNode.InnerHtml + " ";
                            replacementText.Add(outer);
                            nodesToRemove.Add(nextSiblingNode);
                            if (nextSiblingNode.Attributes.Any(n => n.Name == "grv-usedalready"))
                            {
                                nextSiblingNode.Attributes["grv-usedalready"].Value = "yes";
                            }
                            else
                            {
                                nextSiblingNode.Attributes.Add("grv-usedalready", "yes");
                            }

                            nextSiblingNode = nextSiblingNode.NextSibling;
                        }
                    }
                }
                // otherwise
                else if (!string.IsNullOrEmpty(child.InnerHtml))
                {
                    nodesToReturn.Add(child.InnerHtml);
                }
            }

            // flush out anything still remaining
            if (replacementText.Count > 0)
            {
                var txt = string.Join("", replacementText);
                nodesToReturn.Add(txt);
            }

            foreach (var n in nodesToRemove)
            {
                n.Remove();
            }

            return nodesToReturn;
        }

        private static void ReplaceWithPara(HtmlNode div)
        {
            var divContent = div.InnerHtml;
            div.ParentNode?.ReplaceChild(HtmlNode.CreateNode("<p>" + divContent + "</p>"), div);
        }
    }
}
