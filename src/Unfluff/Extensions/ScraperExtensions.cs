﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;

namespace Unfluff.Extensions
{
    internal static class ScraperExtensions
    {
        private static readonly char[] Separators = { '\n', ',', '.', ' ', '\r', '\t' };

        internal static IEnumerable<HtmlNode> SelectNodesByPath(this HtmlNode node, string xPath) => 
            node.SelectNodes(xPath)?.ToList() ?? new List<HtmlNode>();

        internal static bool ContainsAnyWord(this string passString, IEnumerable<string> checkList) =>
            (
                from x in checkList
                let words = passString.Split(Separators, StringSplitOptions.RemoveEmptyEntries)
                where words.Any(word => word.Equals(x, StringComparison.InvariantCultureIgnoreCase))
                select x
            ).Any();

        internal static bool EndsWithAnyWord(this string passString, IEnumerable<string> checkList) => 
            checkList.Any(word => passString.ToLower().EndsWith(word.ToLower()));

        internal static bool IsEqualToAnyWord(this string passString, IEnumerable<string> checkList) =>
            checkList.Any(word => string.Equals(word, passString, StringComparison.CurrentCultureIgnoreCase));
    }
}
