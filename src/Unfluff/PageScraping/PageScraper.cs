﻿using System;
using System.IO;
using System.Text;
using HtmlAgilityPack;
using Unfluff.Helpers;
using Unfluff.Models;
using Unfluff.Utilities;

namespace Unfluff.PageScraping
{
    public class PageScraper
    {
        private readonly ScrapingHelper scrapingHelper;
        private readonly int byteLimit;

        public PageScraper()
        {
            byteLimit = 10 * 1024 * 1024;
            var stopWords = new StopWords();
            scrapingHelper = new ScrapingHelper(stopWords);
        }

        public ScraperResult Process(string html, string originalUrl, Language language = Language.English)
        {
            var size = Encoding.UTF8.GetByteCount(html);

            if (size > byteLimit) throw new Exception("Supplied string is too large. Limit is 10MB.");

            var doc = new HtmlDocument();

            doc.LoadHtml(html);

            return GetResult(doc, originalUrl, language);
        }

        public ScraperResult Process(Stream stream, string originalUrl, Language language = Language.English)
        {
            var doc = new HtmlDocument();
            doc.Load(stream);

            return GetResult(doc, originalUrl, language);
        }

        private ScraperResult GetResult(HtmlDocument document, string url, Language language)
        {
            var result = new ScraperResult
            {
                Title = ScrapingHelper.GetTitle(document),
                Description = ScrapingHelper.GetDescription(document),
                CanonicalLink = ScrapingHelper.GetCanonicalLink(document, url),
                Tags = ScrapingHelper.GetTags(document),
                Favicon = ScrapingHelper.GetFavicon(document),
                Keywords = ScrapingHelper.GetKeywords(document),
                Image = ScrapingHelper.GetImage(document),
                Language = ScrapingHelper.GetLanguage(document),
                Author = ScrapingHelper.GetAuthor(document),
                Copyright = ScrapingHelper.GetCopyright(document),
                PublishedDate = ScrapingHelper.GetPublishedDate(document),
                Publisher = ScrapingHelper.GetPublisher(document)
            };

            // Step 1: Clean the doc
            Cleaner.CleanArticle(document.DocumentNode);

            // Step 2: Find the doc node with the best text
            var topNode = scrapingHelper.GetTopNode(document, language);

            // Step 3: Extract text, videos, images
            result.Links = scrapingHelper.GetLinks(topNode, language, result.CanonicalLink);
            result.Videos = ScrapingHelper.GetVideos(topNode);
            result.Text = scrapingHelper.GetText(topNode, language);

            return result;
        }
    }
}
