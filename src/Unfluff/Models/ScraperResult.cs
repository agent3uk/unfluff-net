﻿using System;
using System.Collections.Generic;

namespace Unfluff.Models
{
    public class ScraperResult
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string CanonicalLink { get; set; }
        public IEnumerable<string> Tags { get; set; }
        public string Favicon { get; set; }
        public string Language { get; set; }
        public string Text { get; set; }
        public IEnumerable<Video> Videos { get; set; }
        public IEnumerable<string> Keywords { get; set; }
        public string Image { get; set; }
        public string Author { get; set; }
        public string Publisher { get; set; }
        public DateTime PublishedDate { get; set; }
        public string Copyright { get; set; }
        public IDictionary<string, string> Links { get; set; }
    }
}